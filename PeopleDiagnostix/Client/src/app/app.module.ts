import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { ReverserComponent } from './components/reverser/reverser.component';
import { DataService } from './services/data.service';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    ReverserComponent
  ],
  imports: [
    BrowserModule,
    // For two way binding the input field
    FormsModule,
    // Import http module so that we can call the api
    HttpClientModule 
  ],
  // Add providers to be injected into components
  providers: [DataService, HttpClient],
  bootstrap: [AppComponent]
})
export class AppModule { }
