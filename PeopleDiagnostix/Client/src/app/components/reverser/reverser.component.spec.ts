import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReverserComponent } from './reverser.component';

describe('ReverserComponentComponent', () => {
  let component: ReverserComponent;
  let fixture: ComponentFixture<ReverserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReverserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReverserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
