import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'pdx-reverser',
  templateUrl: './reverser.component.html',
  styleUrls: ['./reverser.component.css']
})
export class ReverserComponent implements OnInit {

  values: Array<any> = []; // Stores an array of previously displayed strings
  recent: string; // Stores the most recently reversed string
  value: string; // Stores the string currently in the input box
  validationMessage: string; // Stores a validation error if it exists
  submitting:boolean; // True if waiting for server response, so we can update the view

  // Inject the data service into the controller via DI
  constructor(private dataService: DataService) { }

  // Initialise the component by getting all persisted values from the server
  ngOnInit() {
    this.dataService.getItems().then(result => {
      this.values = result;
    }).catch(() => {
      this.validationMessage = "Server Error";
    });
  }

  // Submit post request to the server, and handle errors
  reverseString(): void {
    this.validationMessage = '';
    this.submitting = true;
    
    this.dataService.reverseString(this.value).then(result => {
      this.recent = result.value;
      // Add the returned string to the values array
      this.values.unshift(result);
      this.value = '';
      this.submitting = false;
    }).catch(err => {
      if(err.status == 400){
        // Validation error; Assign the error message response to validationMessage
        this.validationMessage = err.error.Value[0];
      }
      this.submitting = false;
    });
  }

  // Submit delete request to the server using the given id
  removeItem(id: number) : void {
    this.dataService.removeItem(id).then(() => {
      // On success, remove the deleted item from the values array
      this.values = this.values.filter(item => item.id != id)
    });
  }
}
