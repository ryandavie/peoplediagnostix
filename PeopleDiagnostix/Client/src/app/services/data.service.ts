import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  public static API_ENDPOINT: string;
  
  constructor(private http: HttpClient) {
    // Domains are environment dependant
    DataService.API_ENDPOINT = environment.domain + '/api';
  }

  // Submits a GET request for all items on the server
  public getItems() : Promise<any> {
    return this.http.get(DataService.API_ENDPOINT + '/Reverser').toPromise();
  }

  // Submits a POST request to reverse and store an new string
  public reverseString(value: string): Promise<any> {
    return this.http.post(DataService.API_ENDPOINT + '/Reverser', { value: value }).toPromise();
  }

  // Submits a DELETE request to remove an item
  public removeItem(id: number): Promise<any> {
    return this.http.delete(DataService.API_ENDPOINT + '/Reverser/' + id).toPromise();
  }
}
