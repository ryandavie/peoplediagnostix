﻿using Microsoft.AspNetCore.Mvc;

namespace PeopleDiagnostix.Controllers
{
    public class HomeController : Controller
    {
        /// <summary>
        /// Default route serves the static angular index file.
        /// </summary>
        /// <returns>The angular index page</returns>
        public IActionResult Index()
        {
            return File("~/index.html", "text/html");
        }

        /// <summary>
        /// Catch 404 with a not found page.
        /// </summary>
        /// <returns>The 404 Not Found Page</returns>
        [Route("error/404")]
        public IActionResult Error()
        {
            return File("~/notfound.html", "text/html");
        }
    }
}
