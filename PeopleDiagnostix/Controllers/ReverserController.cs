﻿using Microsoft.AspNetCore.Mvc;
using PeopleDiagnostix.Models;
using System;
using System.Linq;

namespace PeopleDiagnostix.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReverserController : ControllerBase
    {
        private IPdxRepository _pdxRepository;

        // Injects the repository into the controller via dependance injection
        public ReverserController(IPdxRepository repository)
        {
            this._pdxRepository = repository;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>All items from the database</returns>
        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_pdxRepository.GetAllItems());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="item">The item to be reversed and saved</param>
        /// <returns>The reversed item</returns>
        [HttpPost]
        public IActionResult Post(ValueItem item)
        {
            // Reverse value
            var arr = item.Value.ToCharArray();
            Array.Reverse(arr);
            item.Value = new string(arr);

            // Prevent duplicate entries
            if (_pdxRepository.Exists(item.Value))
            {
                // Add a modelstate error and return with http statuscode 400
                ModelState.AddModelError("Value", "Value already exists");
                return BadRequest(ModelState);
            }

            var result = _pdxRepository.AddItem(item);
            if (result == null)
            {
                // Server error
                return StatusCode(500);
            }
            return Ok(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id">The Id of the item to be deleted</param>
        /// <returns>The deleted item</returns>
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var result = _pdxRepository.RemoveItem(id);
            if (result == null)
            {
                // Server error
                return StatusCode(500);
            }
            return Ok(result);
        }
    }
}