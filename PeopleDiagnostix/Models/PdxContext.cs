﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace PeopleDiagnostix.Models
{
    /// <summary>
    /// The entity framework context that the project uses to connect to SQL Server
    /// </summary>
    public class PdxContext : DbContext
    {
        public DbSet<ValueItem> Items { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            // Configure EF to use SQL Server, using connection string from appsettings.json
            IConfigurationRoot configuration = new ConfigurationBuilder()
            .SetBasePath(System.IO.Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json")
            .Build();
            optionsBuilder.UseSqlServer(configuration.GetConnectionString("DefaultConnection"));
        }
    }
}
