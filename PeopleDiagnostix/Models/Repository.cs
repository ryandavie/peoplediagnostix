﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PeopleDiagnostix.Models
{
    // Interface defining the repository which will be injected into the controllers 
    public interface IPdxRepository
    {
        IEnumerable<ValueItem> GetAllItems();
        ValueItem AddItem(ValueItem value);
        ValueItem RemoveItem(int id);
        bool Exists(string value);
    }

    // Concrete instance of the repository based on the entity framework context
    public class PdxRepository : IPdxRepository
    {
        private PdxContext _dbContext;

        public PdxRepository()
        {
            _dbContext = new PdxContext();
        }

        // Adds item to the database
        ValueItem IPdxRepository.AddItem(ValueItem value)
        {
            value.DateAdded = DateTime.UtcNow;
            _dbContext.Items.Add(value);
            _dbContext.SaveChanges();
            return value;
        }

        // Returns all items from the database.Returns an empty list if there are none
        IEnumerable<ValueItem> IPdxRepository.GetAllItems()
        {
            // Return in order of the date the item was added
            return _dbContext.Items.OrderByDescending(x => x.DateAdded).ToList();
        }

        // Removes an item from the database given an id. Returns null if the item doesn't exist
        ValueItem IPdxRepository.RemoveItem(int id)
        {
            var item = _dbContext.Items.FirstOrDefault(x => x.Id == id);
            if(item == null)
            {
                return item;
            }
            _dbContext.Remove(item);
            _dbContext.SaveChanges();
            return item;
        }

        // Returns true if there is an item in the database with a value of the given string
        bool IPdxRepository.Exists(string value)
        {
            return _dbContext.Items.Any(x => x.Value == value);
        }
    }
}
