﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PeopleDiagnostix.Models
{
    /// <summary>
    /// An object storing an id, string and the datetime the item was entered into the database
    /// </summary>
    public class ValueItem
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Enter a Value")]
        public string Value { get; set; }
        public DateTime DateAdded { get; set; }
    }
}
