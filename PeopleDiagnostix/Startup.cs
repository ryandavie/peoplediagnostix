﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PeopleDiagnostix.Models;

namespace PeopleDiagnostix
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            // Add CORS to allow requests from external domains.
            services.AddCors(); 
            // add MVC
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            // Add scoped data repository to the services collection
            services.AddScoped<IPdxRepository, PdxRepository>();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            // Configure CORS to allow me to host the app at ryandavie.com.au
            app.UseCors(builder => 
                builder.WithOrigins("https://ryandavie.com.au").AllowAnyHeader().AllowAnyMethod());

            app.UseHttpsRedirection();
            // Allow access to static files to serve the angular app
            app.UseStaticFiles();
            // Catch error codes
            app.UseStatusCodePagesWithReExecute("/error/{0}");
            // Add the default route to go to HomeController, which will serve the angular app
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
